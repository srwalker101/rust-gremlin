#!/bin/sh

set -e

# Ensure the development dependencies are available
./bin/devsetup.sh

(cd ./apache-tinkerpop-gremlin-server-3.4.1 && ./bin/gremlin-server.sh console)
