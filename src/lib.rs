#![allow(dead_code)]

pub trait Stage {
    fn bytecode(&self) -> &Vec<Vec<String>>;
}

pub struct VerticesDescription {
    bytecode: Vec<Vec<String>>,
}

pub struct Command {
    bytecode: Vec<Vec<String>>,
}

impl Command {
    pub fn new() -> Self {
        Command {
            bytecode: Vec::new(),
        }
    }

    #[allow(non_snake_case)]
    pub fn V(mut self) -> Self {
        self.bytecode.push(vec!["V".to_string()]);
        self
    }

    pub fn has_label<S: Into<String>>(mut self, label: S) -> Self {
        self.bytecode
            .push(vec!["hasLabel".to_string(), label.into()]);
        self
    }

    pub fn has<S: Into<String>>(mut self, key: S, op: Op) -> Self {
        self.bytecode
            .push(vec!["has".to_string(), key.into(), op.to_string()]);
        self
    }

    pub fn order(mut self) -> Self {
        self.bytecode.push(vec!["order".to_string()]);
        self
    }

    pub fn by<S: Into<String>>(mut self, key: S, order: Order) -> Self {
        self.bytecode
            .push(vec!["by".to_string(), key.into(), order.to_string()]);
        self
    }
}

impl Stage for Command {
    fn bytecode(&self) -> &Vec<Vec<String>> {
        &self.bytecode
    }
}

pub enum Order {
    Desc,
}

impl Order {
    pub fn to_string(&self) -> String {
        match self {
            Order::Desc => format!("<Order::Desc: 3>"),
        }
    }
}

pub enum Op {
    GreaterThan(i32),
}

impl Op {
    pub fn to_string(&self) -> String {
        match self {
            Op::GreaterThan(value) => format!("gt({})", value),
        }
    }
}

pub struct Client;

impl Client {
    pub fn new() -> Self {
        Client {}
    }

    pub fn to_bytecode<S>(&self, stage: S) -> Vec<Vec<String>>
    where
        S: Stage,
    {
        stage.bytecode().clone()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn create_bytecode() {
        let client = Client::new();

        let imp = Command::new()
            .V()
            .has_label("person")
            .has("age", Op::GreaterThan(30))
            .order()
            .by("age", Order::Desc);
        let bytecode = client.to_bytecode(imp);

        assert_eq!(
            bytecode,
            vec![
                vec!["V".to_string()],
                vec!["hasLabel".to_string(), "person".to_string()],
                vec!["has".to_string(), "age".to_string(), "gt(30)".to_string()],
                vec!["order".to_string()],
                vec![
                    "by".to_string(),
                    "age".to_string(),
                    "<Order::Desc: 3>".to_string()
                ],
            ]
        );
    }
}
