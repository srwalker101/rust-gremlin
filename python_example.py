#!/usr/bin/env python


from gremlin_python.process.anonymous_traversal import traversal
from gremlin_python.driver.driver_remote_connection import DriverRemoteConnection
from gremlin_python.process.traversal import Order
from gremlin_python.process.traversal import P
from gremlin_python.driver import request
from gremlin_python.driver.serializer import GraphSONMessageSerializer
import uuid


g = traversal().withRemote(DriverRemoteConnection("ws://localhost:8182/gremlin", "g"))

iterator = g.V().hasLabel("person").has("age", P.gt(30)).order().by("age", Order.desc)

message = request.RequestMessage(
    processor="traversal",
    op="bytecode",
    args={"gremlin": iterator, "aliases": {"g": "g"}},
)

serializer = GraphSONMessageSerializer()
request_id = str(uuid.uuid4())

serialized = serializer.serialize_message(request_id, message)
print(serialized)
