#!/bin/bash

set -e

GREMLIN_VERSION=3.4.1

GREMLIN_SERVER_STUB=apache-tinkerpop-gremlin-server-${GREMLIN_VERSION}
GREMLIN_SERVER_ZIPFILE=${GREMLIN_SERVER_STUB}-bin.zip
GREMLIN_SERVER_URL=http://mirrors.ukfast.co.uk/sites/ftp.apache.org/tinkerpop/${GREMLIN_VERSION}/${GREMLIN_SERVER_ZIPFILE}
GREMLIN_SERVER_SHA512=4796332d62a5876790cd51e70ee014ae5ce0e191ffb445f5ffed6186bcfd72c5d140f4542767e78df0fb2e12fb134b6825e5ee63922c0c18e92e3b912c1704ae
GREMLIN_PYTHON_STUB=gremlinpython-${GREMLIN_VERSION}
GREMLIN_PYTHON_ZIPFILE=${GREMLIN_PYTHON_STUB}.tar.gz
GREMLIN_PYTHON_URL=https://files.pythonhosted.org/packages/6c/ac/d14ff4fe63b71305c019583fdf036e5c2be70a452b9cd9617f46eab62880/${GREMLIN_PYTHON_ZIPFILE}
GREMLIN_PYTHON_SHA256=2e644d451d4a2f9664f9c26903588217a331f72d73bf2afe3fb36ea40ffdf7c8

log() {
    echo $* >&2
}

compare_sha() {
	file=$1
	expected=$2
	shasize=$3

	case $OSTYPE in
		darwin*)
			foundsha=$(shasum -a $shasize $file | awk '{print $1}')
		;;

		linux*)
			log "Not implemented yet"
			exit 1
		;;
	esac

	if [[ "${foundsha}" != "${expected}" ]]; then
		log "Error: downloaded file (${file}) SHA not correct (${foundsha} != ${expected})"
		exit 1
	fi
}

log "Setting up the repository for development"

test -d "${GREMLIN_SERVER_STUB}" || {
    log "Cannot find unpack dir"

    test -f ${TMPDIR}/${GREMLIN_SERVER_ZIPFILE} || {
        log "Cannot find downloaded zipfile, downloading from ${GREMLIN_SERVER_URL}"
        curl -L ${GREMLIN_SERVER_URL} -o ${TMPDIR}/${GREMLIN_SERVER_ZIPFILE}
    }

	compare_sha ${TMPDIR}/${GREMLIN_SERVER_ZIPFILE} ${GREMLIN_SERVER_SHA512} 512

    unzip -qq ${TMPDIR}/${GREMLIN_SERVER_ZIPFILE}
}

test -d "${GREMLIN_PYTHON_STUB}" || {
	log "Cannot find python dir"
	
	test -f ${TMPDIR}/${GREMLIN_PYTHON_ZIPFILE} || {
		log "Cannot find downloaded zipfile, downloading from ${GREMLIN_PYTHON_URL}"
        curl -L ${GREMLIN_PYTHON_URL} -o ${TMPDIR}/${GREMLIN_PYTHON_ZIPFILE}
	}

	compare_sha ${TMPDIR}/${GREMLIN_PYTHON_ZIPFILE} ${GREMLIN_PYTHON_SHA256} 256

	tar xf ${TMPDIR}/${GREMLIN_PYTHON_ZIPFILE}
}
